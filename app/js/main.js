setField();
function setField() {
    let field = document.querySelector('.field');
    let set1 = new Set();
    let set2 = new Set();
    while (set1.size < 10) {
        set1.add(randomNumber(1, 10));
    }
    while (set2.size < 10) {
        set2.add(randomNumber(1, 10));
    }
    let indexes = Array.from(set1).concat(Array.from(set2));
    field.innerHTML = '';
    for (let i = 0; i < indexes.length; i++) {
        field.innerHTML += `<div class="img-box" data-id="${indexes[i]}"><img src="img/${indexes[i]}.jpg" alt="Lenin"/></div>`;
    }
    let imgs = document.querySelectorAll('.img-box');
    let fieldHasSelectedItem = false;
    let selectedId = 0;
    let erasedLenins = 0;
    for (let i = 0; i < imgs.length; i++) {
        imgs[i].addEventListener('click', function () {
            console.log(erasedLenins);
            if (fieldHasSelectedItem) {
                if (imgs[i].dataset.id == selectedId && !imgs[i].classList.contains('active')) {
                    document.querySelectorAll(`.img-box[data-id="${selectedId}"]`).forEach((el) => {
                        el.classList.remove('active');
                        el.classList.add('disabled');
                        erasedLenins += 1;
                    });
                    fieldHasSelectedItem = false;
                    selectedId = 0;
                } else {
                    for (let k = 0; k < imgs.length; k++) {
                        imgs[k].classList.remove('active');
                    }
                    imgs[i].classList.add('active');
                    fieldHasSelectedItem = true;
                    selectedId = +imgs[i].dataset.id;
                }
            } else {
                for (let k = 0; k < imgs.length; k++) {
                    imgs[k].classList.remove('active');
                }
                imgs[i].classList.add('active');
                fieldHasSelectedItem = true;
                selectedId = +imgs[i].dataset.id;
            }
            if (erasedLenins == 20) {
                document.querySelector('h1').textContent = "You've erased all Lenins, fucking capitalist!";
                setTimeout(function () {
                    document.querySelector('h1').textContent = "BUT...";
                }, 2000);
                setTimeout(function () {
                    document.querySelector('h1').textContent = "Try again!";
                    setField();
                }, 3000);
            }
        });
    }
}
function randomNumber(min, max) {
    let random = min + Math.random() * (max + 1 - min);
    return Math.floor(random);
}
